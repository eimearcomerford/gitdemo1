﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(gitdemo1.Startup))]
namespace gitdemo1
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
